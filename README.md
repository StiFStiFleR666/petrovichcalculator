# PetrovichCalculator

Приложение представляет собой простой калькулятор построенный по клиент-серверной архитектуре с использованием REST API.
Приложение состоит из двух сервисов: CalculatorServer и CalculatorClient.

_CalculatorClient:_
1. Реализовывает REST API для получения данных из электронной формы.
2. Валидацию входящих данных.
3. Передачу данные в CalculatorServer.
4. Покрывает тестами всё REST API.

_CalculatorServer:_
1. Принимает запросы от CalculatorClient.
2. Производит арифметические вычисления.
3. Возвращает ответ CalculatorClient.
4. Взаимодействует с БД и записывает в неё проведенные операции.
5. Покрывает тестами всю серверную часть.

**Технологические решения:**
1. Взаимодействие CalculatorServer и CalculatorClient происходит с использованием JSON.
2. Для сборки используется maven.
3. БД - MySQL.
4. Для взаимодействия с базой используется Spring Data JPA.
5. Используется Spring framework и Spring Boot.
6. Тесты реализованы с использованием Spring Test (JUnit + Mockito).
7. CalculatorServer и CalculatorClient общаются синхронно.
8. UI формы стилизованы с использованием Bootstrap.

**Для запуска приложения необходимо:**
1. Скопировать файлы из репозитория.
2. Установить БД MySQL + создать дефолтную схему.
3. В application.property сервиса CalculatorServer установить соответствующие значения в следующих полях:
    - spring.datasource.url=
    - spring.datasource.username=
    - spring.datasource.password=
4. Запустить классы ClientApplication и ServerApplication.
5. В адресной строке интернет-обозревателя перейти по адресу: http://localhost:8080/ .
6. В появившейся форме указать числа и операцию (+, -, *, /) и нажать кнопку "Рассчитать".