package com.calculator.client.model.validation;





import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DigitsOnlyValidator implements ConstraintValidator<DigitsOnly, String> {
    @Override
    public void initialize(DigitsOnly constraintAnnotation) {

    }

    @Override
    public boolean isValid(String digits, ConstraintValidatorContext constraintValidatorContext) {
        return digits.matches("[0-9\\.]*");
    }
}
