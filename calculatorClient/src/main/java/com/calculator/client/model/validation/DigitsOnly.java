package com.calculator.client.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DigitsOnlyValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DigitsOnly {
    String message() default "{DigitsOnly}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
