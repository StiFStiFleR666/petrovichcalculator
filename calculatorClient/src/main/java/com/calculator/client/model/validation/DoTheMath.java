package com.calculator.client.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DoTheMathValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DoTheMath {
    String message() default "{DoTheMath}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
