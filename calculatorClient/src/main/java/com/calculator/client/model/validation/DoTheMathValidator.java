package com.calculator.client.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DoTheMathValidator implements ConstraintValidator<DoTheMath, String> {
    @Override
    public void initialize(DoTheMath constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.matches("[*\\-\\+/]{0,1}");
    }
}
