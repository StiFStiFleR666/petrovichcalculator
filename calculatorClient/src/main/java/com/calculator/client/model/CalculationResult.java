package com.calculator.client.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@JsonAutoDetect
public class CalculationResult {

    private static String request;
    private static Date requestTime;
    private static Long calculationTime;
    private static Float result;

    public CalculationResult() {
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Long getCalculationTime() {
        return calculationTime;
    }

    public void setCalculationTime(Long calculationTime) {
        this.calculationTime = calculationTime;
    }

    public Float getResult() {
        return result;
    }

    public void setResult(Float result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CalculationResult{" +
                "request='" + request + '\'' +
                ", requestTime=" + requestTime +
                ", calculationTime=" + calculationTime +
                ", result=" + result +
                '}';
    }
}
