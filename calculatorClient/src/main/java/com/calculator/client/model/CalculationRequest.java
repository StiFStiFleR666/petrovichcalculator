package com.calculator.client.model;

import com.calculator.client.model.validation.DigitsOnly;
import com.calculator.client.model.validation.DoTheMath;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotEmpty;


@Component
@JsonAutoDetect
public class CalculationRequest {

    @NotEmpty(message = "Поле не должно быть пустым!")
    @DigitsOnly
    public String number1;

    @NotEmpty(message = "Поле не должно быть пустым!")
    @DoTheMath
    public String operator;

    @DigitsOnly
    @NotEmpty(message = "Поле не должно быть пустым!")
    public String number2;

    public Long requestTime;

    public CalculationRequest() {

    }

    public String getNumber1() {
        return number1;
    }

    public void setNumber1(String number1) {
        this.number1 = number1;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getNumber2() {
        return number2;
    }

    public void setNumber2(String number2) {
        this.number2 = number2;
    }

    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }

    @Override
    public String toString() {
        return "CalculationRequest{" +
                "number1='" + number1 + '\'' +
                ", operator='" + operator + '\'' +
                ", number2='" + number2 + '\'' +
                ", requestTime=" + requestTime +
                '}';
    }
}
