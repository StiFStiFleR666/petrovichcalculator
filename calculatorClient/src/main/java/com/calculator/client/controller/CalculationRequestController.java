package com.calculator.client.controller;

import com.calculator.client.model.CalculationRequest;
import com.calculator.client.model.CalculationResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

@Controller
@RequestMapping("/")
public class CalculationRequestController implements WebMvcConfigurer {

    @Autowired
    CalculationResult calculationResult;

    @Autowired
    CalculationRequest calculationRequest;
    public CalculationRequestController() throws IOException {
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/all").setViewName("all");
    }

    @GetMapping("/")
    public String showForm(CalculationRequest request) {
        return "form";
    }

    @PostMapping(value = "/")
    public String checkUserInfo(@RequestBody @Valid CalculationRequest request, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "form";
        }
        else {
            ObjectMapper mapper = new ObjectMapper();
            StringWriter writer = new StringWriter();
            HttpClient httpClient = HttpClientBuilder.create().build();
            try {
                HttpPost httpRequest = new HttpPost("http://localhost:1717/calc");
                request.setRequestTime(System.currentTimeMillis());
                mapper.writeValue(writer, request);
                StringEntity params = new StringEntity(writer.toString());
                httpRequest.setEntity(params);
                HttpResponse response = httpClient.execute(httpRequest);
            } catch (Exception ex) {
                System.out.println("Что-то пошло не туда...");
            }
            return "redirect:/all";
        }
    }

    @PostMapping(value = "/results")
    @ResponseBody
    public String getResult(@RequestBody String inLine) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(inLine);
        calculationResult = mapper.readValue(reader,CalculationResult.class);
        return "redirect:all";
    }

    @GetMapping("/all")
    public String showAll(CalculationResult s) {
        return "all";
    }

    @GetMapping("/redirect")
    public String redirectForm(CalculationRequest request) {
        return "redirect:/";
    }
}