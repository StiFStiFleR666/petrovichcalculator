import com.calculator.client.controller.CalculationRequestController;
import com.calculator.client.model.CalculationRequest;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.io.IOException;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebMvcTest
public class CalculatorClientTest {

    private MockMvc mockMvc;

    {
        try {
            mockMvc = MockMvcBuilders.standaloneSetup(new CalculationRequestController()).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void showFormTest() {
        try {
            this.mockMvc.perform(get("/")).andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void showAllFormTest() {
        try {
            this.mockMvc.perform(get("/all")).andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void redirectTest() {
        try {
            this.mockMvc.perform(get("/redirect")).andExpect(status().is3xxRedirection());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void resultTest() {
        try {
            this.mockMvc.perform(post("/results")).andExpect(status().isBadRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void formPostTest() throws Exception {
        MockHttpServletRequestBuilder createMessage = post("/").contentType(MediaType.APPLICATION_JSON).content("{\"number1\":\"1\",\t\"operator\":\"+\",\"number2\":\"6\",\"requestTime\":\"1530954010605\"}");
        this.mockMvc.perform(createMessage).andExpect(MockMvcResultMatchers.redirectedUrl("/all"));
    }

    @Test
    public void validationTest() throws Exception {
        MockHttpServletRequestBuilder createMessage = post("/").contentType(MediaType.APPLICATION_JSON).content("{\"number1\":\"1\",\t\"operator\":\"qwe\",\"number2\":\"6\",\"requestTime\":\"1530954010605\"}");
        this.mockMvc.perform(createMessage).andExpect(model().hasErrors());
    }
}
