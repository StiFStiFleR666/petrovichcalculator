import com.calculator.server.model.CalculationRequest;
import com.calculator.server.model.CalculationResult;
import com.calculator.server.service.CalculatorService;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.IOException;
import static junit.framework.TestCase.*;


@SpringBootTest
public class CalculatorServerTest {

    CalculatorService service = new CalculatorService();


    @Test
    public void summTest() {
        CalculationRequest request = new CalculationRequest();
        request.setNumber1(0.1F);
        request.setNumber2(0.9F);
        request.setOperator("+");
        request.setRequestTime(System.currentTimeMillis());
        CalculationResult result = null;
        try {
            result = service.calculate(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(result.getResult(), 1.0F);
    }

    @Test
    public void subTest() {
        CalculationRequest request = new CalculationRequest();
        request.setNumber1(1.1F);
        request.setNumber2(0.1F);
        request.setOperator("-");
        request.setRequestTime(System.currentTimeMillis());
        CalculationResult result = null;
        try {
            result = service.calculate(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(result.getResult(), 1.0F);
    }

    @Test
    public void multTest() {
        CalculationRequest request = new CalculationRequest();
        request.setNumber1(5.0F);
        request.setNumber2(2.0F);
        request.setOperator("*");
        request.setRequestTime(System.currentTimeMillis());
        CalculationResult result = null;
        try {
            result = service.calculate(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(result.getResult(), 10.0F);
    }

    @Test
    public void devTest() {
        CalculationRequest request = new CalculationRequest();
        request.setNumber1(5.0F);
        request.setNumber2(2.0F);
        request.setOperator("/");
        request.setRequestTime(System.currentTimeMillis());
        CalculationResult result = null;
        try {
            result = service.calculate(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(result.getResult(), 2.5F);
    }

    @Test
    public void JsonSerializationTest() {
        CalculationRequest request = new CalculationRequest();
        request.setNumber1(1F);
        request.setNumber2(1F);
        request.setOperator("+");
        request.setRequestTime(1530954010605L);
        String s = "{\"number1\":\"1\",\t\"operator\":\"+\",\"number2\":\"1\",\"requestTime\":\"1530954010605\"}";
        try {
            assertEquals(request, service.makeCalculationRequest(s));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void JsonDesirializationText() throws IOException {
        CalculationResult result = new CalculationResult();
        result.setId(1L);
        result.setRequestTime(1530954010605L);
        result.setCalculationTime(9307170L);
        result.setResult(1F);
        result.setRequest("1*1");
        String s = "{\"request\":\"1*1\",\"requestTime\":1530954010605,\"calculationTime\":9307170,\"result\":1.0}";
        assertEquals(s, service.makeCalculationResultJson(result));
    }
}
