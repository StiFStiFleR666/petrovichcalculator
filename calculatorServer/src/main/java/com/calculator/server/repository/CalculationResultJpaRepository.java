package com.calculator.server.repository;

import com.calculator.server.model.CalculationResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalculationResultJpaRepository extends JpaRepository<CalculationResult, Long> {
}
