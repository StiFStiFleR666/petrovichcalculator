package com.calculator.server.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Objects;

@JsonAutoDetect
public class CalculationRequest {
    public Float number1;
    public String operator;
    public Float number2;
    public Long requestTime;

    public CalculationRequest() {

    }

    public Float getNumber1() {
        return number1;
    }

    public void setNumber1(Float number1) {
        this.number1 = number1;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Float getNumber2() {
        return number2;
    }

    public void setNumber2(Float number2) {
        this.number2 = number2;
    }

    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CalculationRequest)) return false;
        CalculationRequest request = (CalculationRequest) o;
        return Objects.equals(number1, request.number1) &&
                Objects.equals(operator, request.operator) &&
                Objects.equals(number2, request.number2) &&
                Objects.equals(requestTime, request.requestTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number1, operator, number2, requestTime);
    }
}
