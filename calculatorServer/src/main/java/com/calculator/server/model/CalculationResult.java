package com.calculator.server.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Results")
@JsonAutoDetect
public class CalculationResult {

    @Id
    @GeneratedValue
    @JsonIgnore
    @Column(name = "id")
    private Long id;

    @Column(name = "request")
    private String request;

    @Column(name = "requesttime")
    private Long requestTime;

    @Column(name = "calculationtime")
    private Long calculationTime;

    @Column(name = "result")
    private Float result;

    public CalculationResult() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }

    public Long getCalculationTime() {
        return calculationTime;
    }

    public void setCalculationTime(Long calculationTime) {
        this.calculationTime = calculationTime;
    }

    public Float getResult() {
        return result;
    }

    public void setResult(Float result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CalculationResult{" +
                "id=" + id +
                ", request='" + request + '\'' +
                ", requestTime=" + requestTime +
                ", calculationTime=" + calculationTime +
                ", result=" + result +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CalculationResult)) return false;
        CalculationResult result1 = (CalculationResult) o;
        return Objects.equals(id, result1.id) &&
                Objects.equals(request, result1.request) &&
                Objects.equals(requestTime, result1.requestTime) &&
                Objects.equals(calculationTime, result1.calculationTime) &&
                Objects.equals(result, result1.result);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, request, requestTime, calculationTime, result);
    }
}
