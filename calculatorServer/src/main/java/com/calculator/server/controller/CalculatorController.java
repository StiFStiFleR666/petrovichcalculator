package com.calculator.server.controller;

import com.calculator.server.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
public class CalculatorController {

    @Autowired
    CalculatorService calculatorService;

    @PostMapping(value = "/calc")
    @ResponseBody
    public void getRequestAndCalculate(@RequestBody String request) throws IOException {
        calculatorService.calculateAndSaveResult(request);
    }
}
