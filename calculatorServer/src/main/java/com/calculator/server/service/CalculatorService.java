package com.calculator.server.service;

import com.calculator.server.model.CalculationRequest;
import com.calculator.server.model.CalculationResult;
import com.calculator.server.repository.CalculationResultJpaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

@Service
public class CalculatorService {

    @Autowired
    private CalculationResultJpaRepository calculationResultJpaRepository;

    public String calculateAndSaveResult(String object) throws IOException {
        CalculationResult result = calculate(makeCalculationRequest(object));
        saveResult(result);
        String response = makeCalculationResultJson(result);
        sendCalculationResult(response);
        return response;
    }

    public CalculationResult calculate(CalculationRequest calculationRequest) throws IOException {

        CalculationResult result = new CalculationResult();
        switch (calculationRequest.getOperator()) {
            case "*":
                result.setResult(calculationRequest.number1 * calculationRequest.number2);
                break;
            case "+":
                result.setResult(calculationRequest.number1 + calculationRequest.number2);
                break;
            case "-":
                result.setResult(calculationRequest.number1 - calculationRequest.number2);
                break;
            case "/":
                result.setResult(calculationRequest.number1 / calculationRequest.number2);
                break;
            default:
                result.setResult(0F);
        }
        result.setRequestTime(calculationRequest.getRequestTime());
        result.setCalculationTime(System.currentTimeMillis() - calculationRequest.getRequestTime());
        result.setRequest(calculationRequest.number1 + calculationRequest.operator + calculationRequest.number2);
        return result;
    }

    public void saveResult(CalculationResult result) {
        calculationResultJpaRepository.save(result);
    }

    public CalculationRequest makeCalculationRequest(String request) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(request);
        return mapper.readValue(reader, CalculationRequest.class);
    }

    public String makeCalculationResultJson(CalculationResult result) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        mapper.writeValue(writer, result);
        return writer.toString();
    }

    public void sendCalculationResult(String result) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost httpRequest = new HttpPost("http://localhost:8080/results");
            StringEntity params = new StringEntity(result);
            httpRequest.setEntity(params);
            HttpResponse response = httpClient.execute(httpRequest);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
